#!/usr/bin/env python

# Script that tries to convert a csv file with the format Artist,Track,Datetimestring
# into a tsv file that libreimport.py can handle
# Usage: ./csv2tsv.py inputfile > outputfile
import sys, time
from datetime import datetime

try:
    data = sys.argv[1]
except:
    exit('Usage: ./csv2tsv.py inputfile > outputfile')

errorlog = open('csv2tsv.log', 'a')

for line in file(data):
    try:
        #replace all ", " with a tab to get them out of the way for now
        tmpline = line.strip("\n").replace(", ", "\t")

        #split line at commas
        artist, track, date = tmpline.split(",")

        # convert tabs back to ", "
        artist = artist.replace("\t", ", ")
        track = track.replace("\t", ", ")
        date = date.replace("\t", ", ")

        #convert time string to epoch (discard timezone)
        dt = datetime.strptime(date[:-6], '%a, %d %b %Y %H:%M:%S')
        timestamp = int(time.mktime(dt.timetuple()))

        #print line in format that should be accepted by libreimport.py
        print("%s\t%s\t%s\t\t\t\t" % (timestamp, track, artist))
    except:
        # all errors will probably be caused by commas without a space behind it
        # as they will look like delimiters to this script
        errorlog.write(line)

errorlog.close()
