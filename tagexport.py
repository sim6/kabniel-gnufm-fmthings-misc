#!/usr/bin/env python

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Script for exporting tagged items through audioscrobbler API.
Usage: tagexport.py -u USER [-s SOURCESERVER] [-t TARGETSERVER]
"""

import sys, urllib, urllib2, time, hashlib, json, getpass
from optparse import OptionParser
import xml.etree.ElementTree as ET

__version__ = "0.2"

def get_options(parser):
    """ Define command line options."""
    parser.add_option("-u", "--user", dest="susername", default=None,
                      help="User name for source account.")
    parser.add_option("-s", "--server", dest="sserver", default="last.fm",
                      help="Server to fetch tags from, default is last.fm")
    parser.add_option("-t", "--targetserver", dest="tserver", default=None,
                      help="Server to send tags to")
    parser.add_option("-y", "--targetuser", dest="tusername", default=None,
                      help="User name for target account")
    parser.add_option("-p", "--targetpassword", dest="tpassword", default=None,
                      help="Password for target user name")
    parser.add_option("-i", "--inputfile", dest="inputfile", default=None,
                      help="File to load tag data from, data should be tab-separated in the form of Tag\tArtist\tAlbum\tTrack")
    parser.add_option("-o", "--outputfile", dest="outputfile", default=None,
                      help="File to save tag data to")

    options, args = parser.parse_args()

    if not options.susername and not options.inputfile:
        sys.exit("User name not specified, see --help")

    if options.tserver and not options.tusername:
        options.tusername = raw_input("Please enter username for " + options.tserver + ": ")
    if options.tserver and not options.tpassword:
        options.tpassword = getpass.getpass(prompt="Please enter password for " + options.tserver + ": ")

    return options.susername, options.sserver, options.tusername, options.tserver, options.tpassword, options.inputfile, options.outputfile

def gettoptags(server, username):
    if server == 'last.fm':
        baseurl = 'http://ws.audioscrobbler.com/2.0/?'
        urlvars = dict(method='user.gettoptags',
                       api_key='e38cc7822bd7476fe4083e36ee69748e',
                       user=username)
    else:
        if server[:7] != 'http://':
            server = 'http://%s' % server
        baseurl = server + '/2.0/?'
        urlvars = dict(method='user.gettoptags',
                       api_key=('tagexport.py-%s' % __version__).ljust(32, '-'),
                       user=username,
                       limit=200)

    url = baseurl + urllib.urlencode(urlvars)
    try:
        f = urllib2.urlopen(url)
    except Exception, e:
        print "Exception occured: %s" % (e)

    response = f.read()
    return response

def parsetoptags(response):
    tags = []
    xmlpage = ET.fromstring(response)

    taglist = xmlpage.getiterator('tag')

    for tagelement in taglist:
        tags.append(tagelement.find('name').text)

    return tags

def getpersonaltags(server, username, tag, type):
    if server == 'last.fm':
        baseurl = 'http://ws.audioscrobbler.com/2.0/?'
        urlvars = dict(method='user.getpersonaltags',
                       api_key='e38cc7822bd7476fe4083e36ee69748e',
                       user=username,
                       taggingtype=type,
                       tag=tag)
    else:
        if server[:7] != 'http://':
            server = 'http://%s' % server
        baseurl = server + '/2.0/?'
        urlvars = dict(method='user.getpersonaltags',
                       api_key=('tagexport.py-%s' % __version__).ljust(32, '-'),
                       user=username,
                       taggingtype=type,
                       tag=tag,
                       limit=200)

    url = baseurl + urllib.urlencode(urlvars)
    try:
        f = urllib2.urlopen(url)
    except Exception, e:
        print "Exception occured: %s" % (e)

    response = f.read()
    return response

def parsepersonaltags(response, type):
    xmlpage = ET.fromstring(response)
    tagitems = []

    try:
        tagname = xmlpage.find('taggings').attrib.get('tag')
    except:
        return tagitems

    if type == 'track':
        tracklist = xmlpage.getiterator('track')

        for trackelement in tracklist:
            track = trackelement.find('name').text
            artist = trackelement.find('artist').find('name').text
            tagitems.append([tagname, artist, "", track])

    elif type == 'album':
        albumlist = xmlpage.getiterator('album')

        for albumelement in albumlist:
            album = albumelement.find('name').text
            artist = albumelement.find('artist').find('name').text
            tagitems.append([tagname, artist, album, ""])

    elif type == 'artist':
        artistlist = xmlpage.getiterator('artist')

        for artistelement in artistlist:
            artist = artistelement.find('name').text
            tagitems.append([tagname, artist, "", ""])

    return tagitems

def addtags(server, sessionkey, tagitem):
    if server[:7] != 'http://':
        server = 'http://%s' % server

    tag = tagitem[0].encode('utf-8')
    artist = tagitem[1].encode('utf-8')
    album = tagitem[2].encode('utf-8')
    track = tagitem[3].encode('utf-8')


    if artist:
        if album:
            if track:
                type = 'track'
            else:
                type = 'album'
        else:
            if track:
                type = 'track'
            else:
                type = 'artist'

    postdata = dict(method=type+'.addtags', artist=artist, album=album, track=track, sk=sessionkey, format='json', tags=tag)

    req = urllib2.Request(server + '/2.0/', urllib.urlencode(postdata))
    response = urllib2.urlopen(req)
    try:
        status = json.load(response)['lfm']['status']

        if status == 'ok':
            if type == 'track':
                return "Tagged track %s - %s with %s" % (artist, track, tag)
            elif type == 'album':
                return "Tagged album %s - %s with %s" % (artist, album, tag)
            elif type == 'artist':
                return "Tagged artist %s with %s" % (artist, tag)
    except:
        # We dont even get here, as gnufm gives an "ok" response even when failing
        return "Failed, item probably not in target db, we need to fix this"

def auth(fmserver, user, password):
    if fmserver[:7] != 'http://':
        fmserver = 'http://%s' % fmserver

    user=user.lower()
    passmd5 = hashlib.md5(password).hexdigest()
    token = hashlib.md5(user+passmd5).hexdigest()
    getdata = dict(method='auth.getMobileSession', username=user, authToken=token, format='json')
    req = fmserver + '/2.0/?' + urllib.urlencode(getdata)
    response = urllib2.urlopen(req)
    try:
        jsonresponse = json.load(response)
        sessionkey = jsonresponse['session']['key']
    except:
        sys.exit("Could not get sessionkey, wrong user/password?")

    return sessionkey

def main(sserver, susername, tserver, tusername, tpassword, inputfile, outputfile):
    tagitems = []

    #get tag data from local file
    if inputfile:
        for line in file(inputfile):
            tag, artist, album, track = line.strip("\n").split("\t")
            tagitems.append([tag, artist, album, track])
        print("loaded tag data from file")

    #get tag data from server
    else:
        toptags = parsetoptags(gettoptags(sserver, susername))

        if(toptags):
            if outputfile:
                f = open(outputfile, 'a')
    
            if tserver or outputfile:
                print("Getting data for %d tags" % len(toptags))
    
            #for every top tag
            for tag in toptags:

                #for every tag type
                for type in ["artist", "album", "track"]:

                    ptags = parsepersonaltags(getpersonaltags(sserver, susername, tag, type), type)
                    #for every tagged item
                    for ptag in ptags:

                        #ignore duplicates
                        if ptag not in tagitems:
                            #if no target server or outputfile is specified, print to stdout
                            if not tserver and not outputfile:
                                print(("\t".join(ptag)).encode("utf-8"))
                            #if outputfile is specified, write to file
                            elif not tserver and outputfile:
                                f.write(("\t".join(ptag) + "\n").encode('utf-8'))
                            tagitems.append(ptag)
                    time.sleep(1)
                if tserver or outputfile:
                    print("Got data for tag: %s" % tag)
            if outputfile:
                f.close()
    
    #submit tag data to target server if tserver is specified
    if tserver:
        sk = auth(tserver, tusername, tpassword)

        for tagitem in tagitems:
            print addtags(tserver, sk, tagitem)
            time.sleep(1)

if __name__ == "__main__":
    parser = OptionParser()
    susername, sserver, tusername, tserver, tpassword, inputfile, outputfile = get_options(parser)
    main(sserver, susername, tserver, tusername, tpassword, inputfile, outputfile) 
