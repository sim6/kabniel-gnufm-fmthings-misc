#!/usr/bin/env python

# Script that tries to convert a .scrobbler.log file
# into a tsv file that libreimport.py can handle
# Usage: ./scroblog2tsv.py inputfile > outputfile
import sys

try:
    data = sys.argv[1]
except:
    exit('Usage: ./scroblog2tsv.py inputfile > outputfile')


for line in file(data):
    try:
        tmpline = line.strip("\n")
        artist, album, track, trackpos, duration, rating, timestamp, tmbid = tmpline.split("\t")

        #print line in format that should be accepted by libreimport.py
        print("%s\t%s\t%s\t%s\t%s\t\t" % (timestamp, track, artist, album, tmbid))
    except:
        if line[0] != "#":
            errorlog = open('scroblog2tsv.errors', 'a')
            errorlog.write(line)
            errorlog.close()
