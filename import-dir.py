#!/usr/bin/env python

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os, sys
import psycopg2 as dbms
from mutagen.oggvorbis import OggVorbis

class DirImport:

    def __init__(self, hostname, username, password, database):
        self.conn = dbms.connect(host=hostname, user=username, password=password, database=database)
        
        self.cursor = self.conn.cursor()

    def close(self):
        self.cursor.close()
        self.conn.commit()
        self.conn.close()

    def run(self, baseDir, urlMap):
        for root, subdir, files in os.walk(baseDir, followlinks=True):
            for f in files:
                fullpath = os.path.join(root, f)
                if not os.path.isdir(fullpath):
                    if os.path.splitext(f)[1][1:] == 'ogg':
                        track = self.parseTrack(fullpath, urlMap)
                        if track:
                            self.addArtist(track)
                            self.addAlbum(track)
                            self.addTrack(track)

    def parseTrack(self, fullpath, urlMap):
        track = OggVorbis(fullpath)
        print fullpath
        print urlMap['path']
        print urlMap['host']
        try:
            return     {
                'artist':       track['Artist'][0],
                'artistmbid':   track['musicbrainz_artistid'][0],
                'album':        track['Album'][0],
                'albummbid':    track['musicbrainz_albumid'][0],
                'title':        track['Title'][0],
                'mbid':         track['musicbrainz_trackid'][0],
                'streamurl':    fullpath.replace(urlMap['path'], urlMap['host']),
                'license':      track['license'][0],
                }
        except Exception, e:
            print "couldnt parse metainfo for %s" % fullpath
            print "error: ", e
            return False

    def addArtist(self, track):
        if self.artistExists(track):
            try:
                self.cursor.execute("UPDATE Artist SET mbid=%s, streamable=%s WHERE name=%s", (track['artistmbid'], '1', track['artist']))
                self.conn.commit()
                if verboseUpdated: print "artist updated: %s" % (track['artist'],)
            except Exception, e:
                self.conn.rollback()
                print e
        else:
            try:
                self.cursor.execute("INSERT INTO Artist (name, mbid, streamable) VALUES (%s, %s, %s)", (track['artist'], track['artistmbid'], '1'))
                self.conn.commit()
                if verboseAdded: print "artist added: %s" % (track['artist'],)

            except Exception, e:
                self.conn.rollback()
                print e

    def artistExists(self, track):
        try:
            self.cursor.execute("SELECT name FROM Artist WHERE name = %s ", (track['artist'],))
            if self.cursor.rowcount !=0:
                if verboseExists: print "artist exists: %s" % (track['artist'],)
                return self.cursor.rowcount != 0
        except:
            return False

    def addAlbum(self, track):
        if self.albumExists(track):
            pass #TODO Add UPDATE statement
        else:
            try:
                self.cursor.execute("INSERT INTO Album (name, artist_name, mbid) VALUES (%s, %s, %s)", (track['album'], track['artist'], track['albummbid']))
                self.conn.commit()
                if verboseAdded: print "album added: %s" % (track['album'],)

            except Exception, e:
                self.conn.rollback()
                print e

    def albumExists(self, track):
        try:
            self.cursor.execute("SELECT name, artist_name FROM Album WHERE name = %s AND artist_name = %s", (track['album'], track['artist']))
            if self.cursor.rowcount !=0:
                if verboseExists: print "album exists: %s" % (track['album'],)
                return self.cursor.rowcount !=0
        except:
            return False

    def addTrack(self, track):
        print(track['streamurl'])
        if self.trackExists(track):
            try:
                self.cursor.execute("UPDATE Track SET mbid=%s, streamable=%s, streamurl=%s, license=%s WHERE name=%s AND artist_name=%s AND album_name=%s", (track['mbid'], '1', track['streamurl'], track['license'], track['title'], track['artist'], track['album']))
                self.conn.commit()
                if verboseUpdated: print "track updated: %s" % (track['title'],)
            except Exception, e:
                self.conn.rollback()
                print e
        else:
            try:
                self.cursor.execute("INSERT INTO Track (name, artist_name, album_name, mbid, streamurl, streamable, license) VALUES (%s, %s, %s, %s, %s, %s, %s)", (track['title'], track['artist'], track['album'], track['mbid'], track['streamurl'], '1', track['license']))
                self.conn.commit()
                if verboseAdded: print "track added: %s" % (track['title'],)

            except Exception, e:
                self.conn.rollback()
                print e

    def trackExists(self, track):
        try:
            self.cursor.execute("SELECT name FROM Track WHERE artist_name = %s AND album_name = %s AND name = %s", (track['artist'], track['album'], track['title']))
            if self.cursor.rowcount !=0:
                if verboseExists: print "track exists: %s" % (track['title'],)
                return self.cursor.rowcount !=0
        except:
            return False

if __name__ == '__main__':
    if len(sys.argv) != 8:
        print "Imports metadata of all Ogg Vorbis files found in a dir into a gnu-fm (postgres) db"
        print "Usage: import-dir.py <dbhost> <dbname> <dbuser> <dbpassword> <filehostdomain> <filehostpath> <scanpath>"
        sys.exit(1)
    verboseAdded = True
    verboseExists = True
    verboseUpdated = True
    #dbhost = 'localhost'
    dbhost = sys.argv[1]
    #db = 'gnufm'
    db = sys.argv[2]
    #dbuser = 'gnufm'
    dbuser = sys.argv[3]
    #dbpass = 'gnufmpass'
    dbpass = sys.argv[4]
    #urlMap = {'host':'http://filehost.lan', 'path':'/home/gnufm/hostfolder'}
    filehostDomain = sys.argv[5]
    filehostPath = sys.argv[6]
    filehostPath = os.path.abspath(filehostPath)
    urlMap = {'host':filehostDomain, 'path':filehostPath.rstrip('/')}
    #scanPath = '/home/gnufm/hostfolder/music'
    scanPath = sys.argv[7]
    if scanPath == '.':
        scanPath = os.environ['PWD']


    importer = DirImport(dbhost, dbuser, dbpass, db)

    importer.run(scanPath, urlMap)

    importer.close()
