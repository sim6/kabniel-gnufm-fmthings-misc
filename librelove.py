#!/usr/bin/env python

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

'''
Script to love and ban tracks on your libre.fm profile.

USAGE:
    First run: ./librelove writeconf to create an example ~/.libreloveconf, then add name and pass to file.
    then run: ./librelove.py love|unlove|ban|unban|addtag|removetag|nowplaying "artist" "track" "tag"
    Artist and Track can be omitted if you use "mpd" instead of "stdin" as infosource
'''
import json, sys, os, urllib, urllib2, hashlib, ConfigParser
import mpd, subprocess

def libreauth(fmserver, user, password):
    passmd5 = hashlib.md5(password).hexdigest()
    token = hashlib.md5(user+passmd5).hexdigest()
    getdata = dict(method='auth.getMobileSession', username=user, authToken=token, format='json')
    req = fmserver + '/2.0/?' + urllib.urlencode(getdata)
    response = urllib2.urlopen(req)
    try:
        jsonresponse = json.load(response)
        sessionkey = jsonresponse['session']['key']
    except:
        print jsonresponse
        sys.exit(1)

    return sessionkey

def trackinfo(infosource):
    if infosource == 'mpd':
        return mpdinfo()
    else:
        try:
            if len(sys.argv) == 5:
                return sys.argv[2], sys.argv[3], sys.argv[4]
            else:
                return sys.argv[2], sys.argv[3], None
        except:
            print 'ERROR: Not enough parameters, expected something like <action> "<artist>" "<track>"'
            sys.exit(1)

def mpdinfo():
    client = mpd.MPDClient()
    client.connect('localhost', 6600)

    song = client.currentsong()
    
    artist = song.get('artist')
    title = song.get('title')

    client.disconnect()
    
    if artist and title:
        return artist, title
    else:
        print 'ERROR: Artist or Track can not be empty (no song playing?)'
        sys.exit(1)

def libreaction(fmserver, action, trackartist, tracktitle, sessionkey, tag=None):

    if action == 'love':
        libremethod = 'track.love'
        postdata = dict(method=libremethod, artist=trackartist, track=tracktitle, sk=sessionkey, format='json')
    elif action == 'unlove':
        libremethod = 'track.unlove'
        postdata = dict(method=libremethod, artist=trackartist, track=tracktitle, sk=sessionkey, format='json')
    elif action == 'ban':
        libremethod = 'track.ban'
        postdata = dict(method=libremethod, artist=trackartist, track=tracktitle, sk=sessionkey, format='json')
    elif action == 'unban':
        libremethod = 'track.unban'
        postdata = dict(method=libremethod, artist=trackartist, track=tracktitle, sk=sessionkey, format='json')
    elif action == 'nowplaying':
        libremethod = 'track.updatenowplaying'
        postdata = dict(method=libremethod, artist=trackartist, track=tracktitle, duration=300, sk=sessionkey, format='json', api_key='thisisthelibrelovepythonthing124')
    elif action == 'removetag':
        libremethod = 'track.removetag'
        postdata = dict(method=libremethod, artist=trackartist, track=tracktitle, tag=tag, sk=sessionkey, format='json')
    elif action == 'addtag':
        libremethod = 'track.addtags'
        postdata = dict(method=libremethod, artist=trackartist, track=tracktitle, tags=tag, sk=sessionkey, format='json')
    else:
        sys.exit('invalid method: %s' % action)

    req = urllib2.Request(fmserver + '/2.0/', urllib.urlencode(postdata))
    response = urllib2.urlopen(req)
    
    try:
        jsonresponse = json.load(response)
        if action == 'nowplaying':
            print jsonresponse['nowplaying']
        else:
            status = jsonresponse['lfm']['status']

            if status == "ok":
                return True
    except:
        if action == 'nowplaying':
            print jsonresponse['error']
        else:
            return False

def doconfig(action):
    os.chdir(os.path.expanduser('~'))
    config = ConfigParser.ConfigParser()
    if action == 'read':
        if config.read('.libreloveconf'):
            infosource = config.get('main', 'infosource')
            notify = config.getboolean('main', 'notify')

            servernames = config.sections()
            servernames.remove('main')

            servers = {}
            for servername in servernames:
                servers[servername] = {}
                servers[servername]['url'] = config.get(servername, 'url')
                servers[servername]['username'] = config.get(servername, 'username')
                servers[servername]['password'] = config.get(servername,'password')

            return infosource, notify, servers
        else:
            print 'Can not find ~/.libreloveconf, run "./librelove writeconf" to create an example config'
            sys.exit(1)
    elif action == 'write':
        config.add_section('main')
        config.set('main', 'infosource', 'stdin')
        config.set('main', 'notify', False)
        config.add_section('libre.fm')
        config.set('libre.fm', 'url', 'http://libre.fm')
        config.set('libre.fm', 'username', 'yourlibrefmusername')
        config.set('libre.fm', 'password', 'yourlibrefmpassword')

        f = open('.libreloveconf', 'w')
        config.write(f)
        f.close
        os.chmod('.libreloveconf', 0600)

def main():
    artist, title, tag = trackinfo(infosource)

    if artist and title:
        for server in servers:
            url = servers[server]['url']
            username = servers[server]['username']
            password = servers[server]['password']
            sessionkey = libreauth(url, username, password)
            if libreaction(url, action, artist, title, sessionkey, tag):
                if notify:
                    cmd = 'notify-send "%sd %s - %s"' % (action.capitalize(), artist, title)
                    subprocess.Popen(cmd, shell=True)
        sys.exit(0)
    else:
        sys.exit(1)

if __name__ == '__main__':
    try:
        action = sys.argv[1]
    except:
        print 'ERROR: You need to give the script at least an action to do (writeconf, love, unlove, ban, unban, addtag, removetag, nowplaying)'
        sys.exit(1)
   
    if action == 'writeconf':
        doconfig('write')
        print 'NOTICE: You need to fill in your real user name and password in ~/.libreloveconf'
        sys.exit(0)
    else:
        infosource, notify, servers = doconfig('read')

    main()
